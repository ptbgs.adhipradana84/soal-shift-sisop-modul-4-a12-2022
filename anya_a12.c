#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static const char *dirpath = "/home/rocuz99/shift4";
const char *VINEGERE_KEY = "INNUGANTENG";

int isDirectory(const char *path)
{
    struct stat statbuf;
    if (stat(path, &statbuf) != 0)
        return 0;
    return S_ISDIR(statbuf.st_mode);
}

char *get_filename_ext(const char *filename)
{
    char *dot = strrchr(filename, '.');
    if (!dot || dot == filename)
        return "";
    return dot;
}

char rot13_encode_decode(char *c)
{
    if (*c >= 97)
    {
        int temp = *c + 13;
        if (temp > 122)
            temp -= 26;
        return (char)temp;
    }
    else
    {
        int temp = *c + 13;
        if (temp > 90)
            temp -= 26;
        return (char)temp;
    }
}

char atbash_encode_decode(char *c)
{
    if (*c >= 97)
    {
        char temp = 97 + (26 - (*c % 96));
        if (temp > 122)
            temp -= 51;
        return (char)temp;
    }
    else
    {
        char temp = 65 + (26 - (*c % 64));
        if (temp > 90)
            temp -= 51;
        return (char)temp;
    }
}

void vinegere_encode(char str[], char key[], char encoded[])
{
    int str_len = strlen(str);

    for (int i = 0; i < str_len; i++)
    {
        int curr = str[i];
        if (curr >= 97)
            curr += 'A' - 'a';

        int x = (curr + key[i]) % 26;

        if (str[i] >= 97)
            x += 97;
        else
            x += 65;

        char c = x;
        strncat(encoded, &c, 1);
    }
}

void vinegere_decode(char *cipher_text, char *key, char *realText)
{
    int cipher_len = strlen(cipher_text);

    for (int i = 0; i < cipher_len; i++)
    {
        int curr = cipher_text[i];
        if (curr >= 97)
            curr += 'A' - 'a';

        int x = (curr - key[i] + 26) % 26;

        if (cipher_text[i] >= 97)
            x += 97;
        else
            x += 65;

        char c = x;
        strncat(realText, &c, 1);
    }
}

void substring(char *destination, const char *source, int beg, int n)
{
    while (n > 0)
    {
        *destination = *(source + beg);
        destination++;
        source++;
        n--;
    }
    *destination = '\0';
}

void generateKey(char str[], char key[])
{
    int str_len = strlen(str), keyword_len = strlen(VINEGERE_KEY);

    if (str_len <= keyword_len)
    {
        substring(key, VINEGERE_KEY, 0, str_len);
        return;
    }

    for (int i = 0;; i++)
    {
        if (keyword_len == i)
            i = 0;
        if (strlen(key) == str_len)
            break;
        char c = VINEGERE_KEY[i];
        strncat(key, &c, 1);
    }
}

int determineCharType(char *c)
{
    if ((int)*c >= 65 && (int)*c <= 90)
        return 1;
    else if ((int)*c >= 97 && (int)*c <= 122)
        return 2;
    return 0;
}

void encodeDecodeFilenameAnimeku_(char encodedDecodedFileName[], char filename[], char *dot)
{
    for (char *p = filename; p <= dot - 1; p++)
    {
        char char_encoded;
        char *encodedChar = &char_encoded;
        switch (determineCharType(p))
        {
        case 1:;
            char_encoded = atbash_encode_decode(p);
            strncat(encodedDecodedFileName, encodedChar, 1);
            break;
        case 2:;
            char_encoded = rot13_encode_decode(p);
            strncat(encodedDecodedFileName, encodedChar, 1);
            break;
        case 0:
            strncat(encodedDecodedFileName, p, 1);
        }
    }
}

void encodeDecodeFilenameVigenere(char encodedDecodedFileName[], char filename[], char *dot)
{
    char onlyFilename[100] = {0};
    for (char *p = filename; p <= dot - 1; p++)
    {
        strncat(onlyFilename, p, 1);
    }

    char key[strlen(onlyFilename) + 1];
    memset(key, 0, sizeof(key));
    strcpy(key, VINEGERE_KEY);

    generateKey(onlyFilename, key);

    vinegere_encode(onlyFilename, key, encodedDecodedFileName);
}

char *getDirectoryName(const char *path)
{
    char *dirname = strrchr(path, '/');
    return dirname;
}

void addExtension(char *filename, char *dot)
{
    strcat(filename, dot);
}

void encodeDecodeFileAnimeku_(char *filename, char encodedDecodedFileName[])
{
    char *dot = get_filename_ext(filename);
    if (!*dot)
        dot = filename + strlen(filename);
    encodeDecodeFilenameAnimeku_(encodedDecodedFileName, filename, dot);
    addExtension(encodedDecodedFileName, dot);
}

void encodeDecodeFileIAN_(char *filename, char encodedDecodedFileName[])
{
    char *dot = get_filename_ext(filename);
    if (!*dot)
        dot = filename + strlen(filename);
    encodeDecodeFilenameVigenere(encodedDecodedFileName, filename, dot);
    addExtension(encodedDecodedFileName, dot);
}

bool startsWith(const char *a, const char *b)
{
    if (strncmp(a, b, strlen(b)) == 0)
        return 1;
    return 0;
}

void recordToWibuLog(const char *from, char *dirFrom, const char *to, char *dirTo)
{
    if (!(startsWith(from, "/Animeku_") || startsWith(to, "/Animeku_")))
        return;

    FILE *fptr;
    fptr = fopen("./Wibu.log", "a+");

    if (fptr == NULL)
    {
        perror("Error when opening file!");
        exit(1);
    }

    char str[500] = "RENAME ";
    if (startsWith(to, "/Animeku_"))
    {
        strcat(str, "terenkripsi ");
        strcat(str, dirFrom);
        strcat(str, " -> ");
        strcat(str, dirTo);
        fprintf(fptr, "%s", str);
    }
    else if (startsWith(from, "/Animeku_"))
    {
        strcat(str, "terdecode ");
        strcat(str, dirFrom);
        strcat(str, " -> ");
        strcat(str, dirTo);
        fprintf(fptr, "%s", str);
    }
    fprintf(fptr, "%s", "\n");

    fclose(fptr);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

int listFilesRecursively(char *fpath, const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char encodedDecodedFileName[100] = {0};

        char fullPathToName[200];
        strcpy(fullPathToName, fpath);
        strcat(fullPathToName, "/");
        strcat(fullPathToName, de->d_name);

        if (!isDirectory(fullPathToName) && !(strcmp(de->d_name, "..") == 0 || strcmp(de->d_name, ".") == 0))
        {
            if (startsWith(getDirectoryName(path), "/Animeku_"))
            {
                encodeDecodeFileAnimeku_(de->d_name, encodedDecodedFileName);
            }
            else if (startsWith(getDirectoryName(path), "/IAN_"))
            {
                encodeDecodeFileIAN_(de->d_name, encodedDecodedFileName);
            }
        }

        if (strlen(encodedDecodedFileName) != 0)
            res = (filler(buf, encodedDecodedFileName, &st, 0));
        else
            res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;

        char newFpath[200];
        strcpy(newFpath, fpath);
        strcat(newFpath, path);

        char newPath[100];
        strcpy(newPath, "/");
        strcat(newPath, de->d_name);
        listFilesRecursively(newFpath, newPath, buf, filler, offset, fi);
    }

    closedir(dp);
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = listFilesRecursively(fpath, path, buf, filler, offset, fi);

    return res;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    char dirFrom[1000], dirTo[1000];
    sprintf(dirFrom, "%s%s", dirpath, from);
    sprintf(dirTo, "%s%s", dirpath, to);

    recordToWibuLog(from, dirFrom, to, dirTo);

    int res;
    res = rename(dirFrom, dirTo);
    if (res == -1)
        return -errno;

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}