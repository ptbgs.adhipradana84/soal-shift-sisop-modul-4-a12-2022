# soal-shift-sisop-modul-4-A12-2022

Member
- Christhoper Marcelino Mamahit (5025201249)
- I Putu Bagus Adhi Pradana (5025201010)
- Yusron Nugroho Aji (5025201138)

## Soal 1
```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};
```
Diatas adalah struktur fuse yang digunakan.

```c
int isDirectory(const char *path)
{
    struct stat statbuf;
    if (stat(path, &statbuf) != 0)
        return 0;
    return S_ISDIR(statbuf.st_mode);
}
```
Memastikan jika direktori ada.

```c
char *get_filename_ext(const char *filename)
{
    char *dot = strrchr(filename, '.');
    if (!dot || dot == filename)
        return "";
    return dot;
}

```
Mendapatkan nama dari file.

Untuk poin a dimana jika direktori ada maka akan dilakukan decode dibawah :

- Semua file dengan huruf besar :
  ```c
  char rot13_encode_decode(char *c)
  {
      if (*c >= 97)
      {
          int temp = *c + 13;
          if (temp > 122)
              temp -= 26;
          return (char)temp;
      }
      else
      {
          int temp = *c + 13;
          if (temp > 90)
              temp -= 26;
          return (char)temp;
      }
  }
  ```

- Semua file dengan huruf kecil
  ```c
  char atbash_encode_decode(char *c)
  {
      if (*c >= 97)
      {
          char temp = 97 + (26 - (*c % 96));
          if (temp > 122)
              temp -= 51;
          return (char)temp;
      }
      else
      {
          char temp = 65 + (26 - (*c % 64));
          if (temp > 90)
              temp -= 51;
          return (char)temp;
      }
  }
  ```
Dibawah program digunakan untuk pendecodean dan encode.
```c
void encodeDecodeFileAnimeku_(char *filename, char encodedDecodedFileName[])
{
    char *dot = get_filename_ext(filename);
    if (!*dot)
        dot = filename + strlen(filename);
    encodeDecodeFilenameAnimeku_(encodedDecodedFileName, filename, dot);
    addExtension(encodedDecodedFileName, dot);
}

void encodeDecodeFilenameAnimeku_(char encodedDecodedFileName[], char filename[], char *dot)
{
    for (char *p = filename; p <= dot - 1; p++)
    {
        char char_encoded;
        char *encodedChar = &char_encoded;
        switch (determineCharType(p))
        {
        case 1:;
            char_encoded = atbash_encode_decode(p);
            strncat(encodedDecodedFileName, encodedChar, 1);
            break;
        case 2:;
            char_encoded = rot13_encode_decode(p);
            strncat(encodedDecodedFileName, encodedChar, 1);
            break;
        case 0:
            strncat(encodedDecodedFileName, p, 1);
        }
    }
}
```
Program di bawah digunakan untuk memenuhi poin b dan c dimana jika terjadi rename dimana menambahkan `Animeku_` pada nama maka akan dilahkukan encode pada file yang ada pada direktori yang bersangkutan, sementara jika direktori yang di rename menjadi tidak ter-encode akan dilahkukan pendecodedan terhadap file yg ada pada direktori tersebut.
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = listFilesRecursively(fpath, path, buf, filler, offset, fi);

    return res;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    char dirFrom[1000], dirTo[1000];
    sprintf(dirFrom, "%s%s", dirpath, from);
    sprintf(dirTo, "%s%s", dirpath, to);

    recordToWibuLog(from, dirFrom, to, dirTo);

    int res;
    res = rename(dirFrom, dirTo);
    if (res == -1)
        return -errno;

    return res;
}

```

Untuk memenuhi poin d digunakan program berikut untuk melahkukan pencatatan pada wibu.log
```c
void recordToWibuLog(const char *from, char *dirFrom, const char *to, char *dirTo)
{
    if (!(startsWith(from, "/Animeku_") || startsWith(to, "/Animeku_")))
        return;

    FILE *fptr;
    fptr = fopen("./Wibu.log", "a+");

    if (fptr == NULL)
    {
        perror("Error when opening file!");
        exit(1);
    }

    char str[500] = "RENAME ";
    if (startsWith(to, "/Animeku_"))
    {
        strcat(str, "terenkripsi ");
        strcat(str, dirFrom);
        strcat(str, " -> ");
        strcat(str, dirTo);
        fprintf(fptr, "%s", str);
    }
    else if (startsWith(from, "/Animeku_"))
    {
        strcat(str, "terdecode ");
        strcat(str, dirFrom);
        strcat(str, " -> ");
        strcat(str, dirTo);
        fprintf(fptr, "%s", str);
    }
    fprintf(fptr, "%s", "\n");

    fclose(fptr);
}
```
Untuk memenuhi poin e digunakan potongan program berikut.
```c
int listFilesRecursively(char *fpath, const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char encodedDecodedFileName[100] = {0};

        char fullPathToName[200];
        strcpy(fullPathToName, fpath);
        strcat(fullPathToName, "/");
        strcat(fullPathToName, de->d_name);

        if (!isDirectory(fullPathToName) && !(strcmp(de->d_name, "..") == 0 || strcmp(de->d_name, ".") == 0))
        {
            if (startsWith(getDirectoryName(path), "/Animeku_"))
            {
                encodeDecodeFileAnimeku_(de->d_name, encodedDecodedFileName);
            }
            else if (startsWith(getDirectoryName(path), "/IAN_"))
            {
                encodeDecodeFileIAN_(de->d_name, encodedDecodedFileName);
            }
        }

        if (strlen(encodedDecodedFileName) != 0)
            res = (filler(buf, encodedDecodedFileName, &st, 0));
        else
            res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;

        char newFpath[200];
        strcpy(newFpath, fpath);
        strcat(newFpath, path);

        char newPath[100];
        strcpy(newPath, "/");
        strcat(newPath, de->d_name);
        listFilesRecursively(newFpath, newPath, buf, filler, offset, fi);
    }

    closedir(dp);
    return 0;
}

```
## Soal 2
```c
void vinegere_encode(char str[], char key[], char encoded[])
{
    int str_len = strlen(str);

    for (int i = 0; i < str_len; i++)
    {
        int curr = str[i];
        if (curr >= 97)
            curr += 'A' - 'a';

        int x = (curr + key[i]) % 26;

        if (str[i] >= 97)
            x += 97;
        else
            x += 65;

        char c = x;
        strncat(encoded, &c, 1);
    }
}

void vinegere_decode(char *cipher_text, char *key, char *realText)
{
    int cipher_len = strlen(cipher_text);

    for (int i = 0; i < cipher_len; i++)
    {
        int curr = cipher_text[i];
        if (curr >= 97)
            curr += 'A' - 'a';

        int x = (curr - key[i] + 26) % 26;

        if (cipher_text[i] >= 97)
            x += 97;
        else
            x += 65;

        char c = x;
        strncat(realText, &c, 1);
    }
}

```
Program diatas digunakan untuk memenuhi poin a yaitu melahkukan decode dan encode direktori dengan awalan `IAN_` 

Sama seperti sebelumnya pada soal 1, poin b dan c menggunakan program yang sama hanya saja pada bagian filename Animeku dan file Animeku diganti dengan berikut.  

```c
void encodeDecodeFilenameVigenere(char encodedDecodedFileName[], char filename[], char *dot)
{
    char onlyFilename[100] = {0};
    for (char *p = filename; p <= dot - 1; p++)
    {
        strncat(onlyFilename, p, 1);
    }

    char key[strlen(onlyFilename) + 1];
    memset(key, 0, sizeof(key));
    strcpy(key, VINEGERE_KEY);

    generateKey(onlyFilename, key);

    vinegere_encode(onlyFilename, key, encodedDecodedFileName);
}

void encodeDecodeFileIAN_(char *filename, char encodedDecodedFileName[])
{
    char *dot = get_filename_ext(filename);
    if (!*dot)
        dot = filename + strlen(filename);
    encodeDecodeFilenameVigenere(encodedDecodedFileName, filename, dot);
    addExtension(encodedDecodedFileName, dot);
}

```

## Soal 3
